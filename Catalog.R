library(raster)
library(lidR)

#Creating lascatalog from a folder
ctg <- readLAScatalog("F:/thesis_joep/Data/Ochsenkopf", progress=T)
 
crs <- sp::CRS("+init=epsg:32632")
projection(ctg) <- crs

#name output files
opt_output_files(ctg) <- "F:/thesis_joep/Data/output/ochs_clas_{ORIGINALFILENAME}"


#classify all catalog tiles
opt_stop_early(ctg)<- F
classctg<- lasground(ctg, csf())

opt_
#create dem

classctgochs <- readLAScatalog(("F:/thesis_joep/Data/output/"))
classctgsimm <- readLAScatalog('F:/thesis_joep/Data/simmerath results')

demsochs <- grid_terrain(classctgochs, res=0.11, tin())
demssimm <- grid_terrain(classctgsimm, res=0.11, tin())

projection(demsochs) <- crs
projection(demssimm) <- crs
# write new hillshader
Rasterhillshade = function(DEM, resolution){
  slope_ras<-terrain(DEM, opt='slope', unit='radians', neighbours = 8)
  aspect_ras<-terrain(DEM, opt='aspect', unit='radians',neighbours= 8)
  rasterprint <- hillShade(slope_ras, aspect_ras, 45, 315)
  return(rasterprint)
  
}


#process each of the layers
ochsenkopf <- Rasterhillshade(demsochs,0.11)
simmerath <-  Rasterhillshade(demssimm,0.11)

writtenraster <- writeRaster(ochsenkopf, 'ochsenkopf.tif', overwrite=TRUE)
writtenraster2 <- writeRaster(simmerath, 'simmerath.tif', overwrite=TRUE)

plot(ochsenkopf, col=gray(0:100/100))
plot(simmerath)
